const express = require('express');
const app = express();
const schools = require('./routes/schools');
const morgan = require('morgan');
const adminForms = require('./routes/adminForms');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const sql = require('./db.js');
const users = require('./routes/users');
const teachers = require('./routes/teacher');
const students = require('./routes/student');
const management = require('./routes/management');


//middlewares
app.use(express.json());
app.use(express.urlencoded({ extended : true}));
app.use(express.static('public'));
app.use(cors());
app.use(morgan('dev'));
app.use(expressJwt({secret: 'ZxyE4rxcvjsThh3ldTGFFvj9hv'}).unless({path: ['/api/login']}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ type: 'application/json' }));
app.use('/api/users',users);
app.use('/api/schools',schools);
app.use('/api/adminForms',adminForms);
app.use('/api/teachers',teachers);
app.use('/api/students',students);
app.use('/api/management',management);

app.post('/api/login',(req,res)=>{
    let body = req.body;
    sql.query("SELECT * FROM `t_user` WHERE `user_name` = ?",body.userName,(err,result)=>{
        if(err){
            res.send({
                status: 500,
                access_token: null,
                content : [{}],
                message:err.message
            })
        }
        else{
            if(result.length !== 1){
                res.send({
                    status: 201,
                    access_token: null,
                    content : [{}],
                    message:"UserName doesn't exists :)"
                })
            }
            else{
                if(result[0].password == body.password){
                    let token = jwt.sign({userId: result[0].user_id_fk, role: result[0].role_fk},'ZxyE4rxcvjsThh3ldTGFFvj9hv',
                    {expiresIn: '2h'});
                    res.send({
                        status:200,
                        access_token: token,
                        content: [{
                            userId: result[0].user_id_fk,
                            role: result[0].role_fk
                        }],
                        message: "success"
                    })
                }
                else{
                    res.send({
                        status: 202,
                        access_token: null,
                        content : [{}],
                        message:"User Authentication Failed"
                    })
                }
            }
        }
    })
})


app.get('/',(req,res)=>{
    res.send("heyyy");
})

//Error handler if no routes found
app.use((req,res,next)=>{
    const error = new Error('No Service');
    error.status(404);
    next(error);  
})

//Global Error handler
app.use((error,req,res,next)=>{
    res.status(error.status || 500);
    res.json({
        error:{
            message:error.message
        }
    })
})
const port = process.env.PORT || 3000;
app.listen(port,()=> console.log(`Listening on port ${port}...`));