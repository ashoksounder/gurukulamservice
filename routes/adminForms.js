const express = require('express');
const router = express.Router();
const sql = require('../db.js');
const dateFormat = require('dateformat');

var getDates = function (startDate, endDate) {
    var dates = [],
        currentDate = startDate,
        addDays = function (days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
    while (currentDate <= endDate) {
        dates.push(currentDate);
        currentDate = addDays.call(currentDate, 1);
    }
    return dates;
};

//routes for roles
router.get("/roles", (req, res) => {
    sql.query(`SELECT * FROM t_roles`, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                message: err.message,
                content: []
            })
        }
        else {
            res.send({
                status: 200,
                message: "Success",
                content: result
            })
        }
    });
})

//routes for standards form
router.get("/standards", (req, res) => {
    sql.query(`SELECT std_id,std_name,false as editFlag,false as spinnerFlag from t_standard`, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/standards", (req, res) => {
    var std_name = req.body.std_name;
    sql.query(`INSERT INTO t_standard (std_id , std_name) VALUES (NULL, ?)`, std_name, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Successfully posted the standard"
            })
        }
    })
})

router.patch("/standards/:id", (req, res) => {
    var id = req.params.id;
    var name = req.body.std_name;
    sql.query(`UPDATE t_standard SET std_name = ? WHERE std_id = ? `, [name, id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Successfully updated the standard"
            })
        }
    });
})


//routes for department forms
router.get("/departments", (req, res) => {
    sql.query(`SELECT dept_id,dept_name,false as editFlag,false as spinnerFlag from t_department`, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/departments", (req, res) => {
    var dept_name = req.body.dept_name;
    sql.query(`INSERT INTO t_department (dept_id , dept_name) VALUES (NULL, ?)`, dept_name, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Successfully posted the department"
            })
        }
    })
})

router.patch("/departments/:id", (req, res) => {
    var id = req.params.id;
    var name = req.body.dept_name;
    sql.query(`UPDATE t_department SET dept_name = ? WHERE dept_id = ? `, [name, id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Successfully updated the standard"
            })
        }
    });
})

//routes for subjectActivity forms
router.get("/subjects", (req, res) => {
    sql.query(`SELECT subject_id,subject_name,activity_flag,false as editFlag,false as spinnerFlag from t_subject`, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/subjects", (req, res) => {
    let subject_name = req.body.subject_name;
    let activity_flag = req.body.activity_flag;
    sql.query(`INSERT INTO t_subject (subject_id , subject_name, activity_flag) VALUES (NULL, ?, ?)`, [subject_name, activity_flag], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Successfully posted the data"
            })
        }
    })
})

router.patch("/subjects/:id", (req, res) => {
    var id = req.params.id;
    var name = req.body.subject_name;
    sql.query(`UPDATE t_subject SET subject_name = ? WHERE subject_id = ? `, [name, id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Successfully updated the standard"
            })
        }
    });
})

router.post("/createteachers", (req, res) => {
    let p = req.body;
    let sqlQuery1 = "INSERT INTO t_user_data(user_id, first_name, last_name, dob, gender, degree,joindate, contact1, contact2, email, address1, address2, state, po_code, city) VALUES (?)";
    let sqlQuery2 = "INSERT INTO t_user(user_name, password, role_fk, user_id_fk) VALUES (?)";
    let sqlQuery3 = "INSERT INTO t_teacher_departments (user_fk, dept_fk) VALUES (?)";
    let values1 = ['NULL', p.firstName, p.lastName, p.dob, p.gender,
        p.qualification,p.joindate, p.primaryContact, p.secondaryContact,
        p.mail, p.address1, p.address2,
        p.state, p.postcode, p.city];

    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        sql.query(sqlQuery1, [values1], (err1, result1) => {
            if (err1) {
                sql.rollback(() => {
                    res.send({
                        status: 500,
                        content: [],
                        message: err1.message
                    })
                })
            }
            else {
                let values2 = [p.userName, p.password, p.grade, result1.insertId];
                sql.query(sqlQuery2, [values2], (err2, result2) => {
                    if (err2) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err2.message
                            })
                        })
                    }
                    else {
                        let values3 = [result1.insertId, p.department]
                        sql.query(sqlQuery3, [values3], (err3, result3) => {
                            if (err3) {
                                sql.rollback(() => {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err3.message
                                    })
                                })
                            }
                            else {
                                sql.commit((err4) => {
                                    if (err4) {
                                        sql.rollback(function () {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err4.message
                                            })
                                        });
                                    }
                                    res.send({
                                        status: 200,
                                        content: result1,
                                        message: "Successfully posted the data"
                                    })
                                })
                            }
                        })
                    }
                })
            }
        })
    })
})


router.get("/teachers", (req, res) => {
    sql.query('SELECT t.user_id,t.first_name,t.last_name,CONCAT(t.first_name," ",t.last_name," | ", d.dept_name) as fullName from t_user_data t INNER JOIN t_department d INNER JOIN t_teacher_departments td WHERE td.dept_fk = d.dept_id AND td.user_fk = t.user_id', (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.post("/createclass", (req, res) => {
    let p = req.body;
    console.log(p);
    let sqlQuery1 = " INSERT INTO t_class(class_id, section_name, std_fk, class_teacher_fk, academic_fk) VALUES (?)";
    let sqlQuery2 = "INSERT INTO t_class_subjects(class_fk, group_id, group_name, subject_fk, subject_teacher_fk) VALUES ?";
    let values1 = ['NULL', p.section, p.standard, p.classTeacher, p.academicYear];

    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        sql.query(sqlQuery1, [values1], (err1, result1) => {
            if (err1) {
                sql.rollback(() => {
                    res.send({
                        status: 500,
                        content: [],
                        message: err1.message
                    })
                })
            }
            else {
                let values2 = [];
                for (let i = 0; i < p.groups.length; i++) {
                    let element;
                    for (let j = 0; j < p.groups[i].subjects.length; j++) {
                        element = [];
                        element = [result1.insertId, i + 1, p.groups[i].groupName, p.groups[i].subjects[j].subject, p.groups[i].subjects[j].teacher];
                        values2.push(element);
                    }
                }
                console.log(values2);
                sql.query(sqlQuery2, [values2], (err2, result2) => {
                    if (err2) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err2.message
                            })
                        })
                    }
                    else {
                        sql.commit((err3) => {
                            if (err3) {
                                sql.rollback(function () {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err3.message
                                    })
                                });
                            }
                            res.send({
                                status: 200,
                                content: result1,
                                message: "Successfully posted the data"
                            })
                        })
                    }
                })
            }
        })
    })
})

//routes for session forms
router.get("/sessions", (req, res) => {
    sql.query(`SELECT period_id,period_name,false as editFlag,false as spinnerFlag from t_periods`, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/typeofschedules", (req, res) => {
    sql.query('SELECT schedule_id, schedule_name FROM t_schedules ORDER BY schedule_id ASC', (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.post("/generateschedules", (req, res) => {
    let p = req.body;
    let dates = getDates(new Date(p.dateRange[0]), new Date(p.dateRange[1]));
    let queries = [];
    let sqlQuery = "";
    //query for regular schedule all standards

    dates.forEach(date => {

        if (p.scheduleType == 4) {
            sqlQuery = "INSERT INTO `t_daily_schedules`(`session_id`, `date`, `class_fk`, `schedule_fk`, `period_fk`, `subject_fk`, `status_flag`)\
            SELECT"+
                "'NULL','" +
                dateFormat(date, "isoDate") + "',\
                cp.class_fk,\
                '"+ p.scheduleType + "',\
                cp.period_fk,\
                cs.subject_fk,\
                NULL\
            FROM\
                t_class_periods cp\
            INNER JOIN\
                t_class c ON c.class_id = cp.class_fk\
            INNER JOIN\
                t_standard s ON c.std_fk = s.std_id\
            INNER JOIN\
                t_class_subjects cs ON cs.group_id = cp.group_fk AND cs.class_fk = cp.class_fk\
            WHERE\
                cp.day_fk IN ("+ p.swapDay + ")";
        }
        else if (p.scheduleType == 5 || p.scheduleType == 3) {
            sqlQuery = "INSERT INTO `t_daily_schedules`(`session_id`, `date`, `class_fk`, `schedule_fk`, `period_fk`, `subject_fk`, `status_flag`)\
                        SELECT \
                        NULL,'"+
                dateFormat(date, "isoDate") + "',\
                        c.class_id,'"+
                p.scheduleType +
                "',NULL,NULL,NULL \
                        FROM \
                            t_class c \
                        INNER JOIN \
                            t_standard s ON s.std_id = c.std_fk \
                        WHERE \
                            s.std_id = c.std_fk ";
        }
        else {
            sqlQuery = "INSERT INTO `t_daily_schedules`(`session_id`, `date`, `class_fk`, `schedule_fk`, `period_fk`, `subject_fk`, `status_flag`)\
            SELECT"+
                "'NULL','" +
                dateFormat(date, "isoDate") + "',\
                cp.class_fk,\
                '"+ p.scheduleType + "',\
                cp.period_fk,\
                cs.subject_fk,\
                NULL\
            FROM\
                t_class_periods cp\
            INNER JOIN\
                t_class c ON c.class_id = cp.class_fk\
            INNER JOIN\
                t_standard s ON c.std_fk = s.std_id\
            INNER JOIN\
                t_class_subjects cs ON cs.group_id = cp.group_fk AND cs.class_fk = cp.class_fk\
            WHERE\
                cp.day_fk IN ("+ date.getDay() + ")";
        }

        console.log(sqlQuery);

        if (p.applicableTo == 1) {
            let standardList = [];
            standardList = p.selectedStandards;
            let standardParams = standardList.join(",");
            sqlQuery = sqlQuery + "AND s.std_id IN (" + standardParams + ")";
        }
        if (p.applicableTo == 2) {
            let standardList = [];
            standardList = p.selectedStandards;
            let standardParams = standardList.join(",");
            sqlQuery = sqlQuery + "AND s.std_id NOT IN (" + standardParams + ")";
        }
        if (p.scheduleType == 2) {
            let sessionList = [];
            sessionList = p.selectedSessions;
            let sessionParams = sessionList.join(",");
            sqlQuery = sqlQuery + "AND cp.period_fk IN (" + sessionParams + ")";
        }
        queries.push(sqlQuery);
    })

    let loopLength = queries.length;
    var resFlag = true;
    let resContent;

    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message + "From Transaction"
            });
        }
        else {
            for (let i = 0; i < loopLength; i++) {
                if (resFlag) {
                    sql.query(queries[i], (err, result) => {
                        if (err) {
                            console.log(i);
                            sql.rollback(() => {
                                if (!resContent) {
                                    // res.send({
                                    //     status: 500,
                                    //     content: [],
                                    //     message: err.message + "~From insert in schedule"
                                    // });
                                    resContent = err.message + "~From insert in schedule";
                                }
                            })
                        }
                        else {
                            if (i == (loopLength - 1)) {
                                sql.commit((err) => {
                                    if (err) {
                                        sql.rollback(function () {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err.message + "~From commit"
                                            });
                                        });
                                    }
                                    else {
                                        if (resContent) {
                                            res.send({
                                                status: 200,
                                                content: [],
                                                message: resContent
                                            });
                                        }
                                        else {
                                            res.send({
                                                status: 200,
                                                content: [],
                                                message: "New Schedules were added:)"
                                            });
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        }
    })

})

router.post("/insertschedules", (req, res) => {
    let p = req.body;
    let dates = getDates(new Date(p.dateRange[0]), new Date(p.dateRange[1]));
    let queries = [];
    let standardList = p.selectedStandards;
    let standardParams = null;
    if (standardList.length > 0) {
        standardParams = standardList.join(",");
    }

    let sessionList = p.selectedSessions;
    let sessionParams = null;
    if (sessionList.length > 0) {
        sessionParams = sessionList.join(",");
    }

    dates.forEach(date => {
        let tempObj = "CALL insertschedules('" + dateFormat(date, "isoDate") + "'," + date.getDay() + "," + p.scheduleType + "," + p.applicableTo + "," + standardParams + "," + sessionParams + ");";
        queries.push(tempObj);
    })

    console.log(queries);

    let loopLength = queries.length;
    var resFlag = true;
    let resContent;

    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message + "From Transaction"
            });
        }
        else {
            console.log("Transaction success");
            for (let i = 0; i < loopLength; i++) {
                if (resFlag) {
                    sql.query(queries[i], (err, result) => {
                        if (err) {
                            sql.rollback(() => {
                                if (!resContent) {
                                    resContent = err.message + "~From insert in schedule";
                                    resFlag = false;
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err.message + "~From insert in schedule"
                                    });
                                }
                            })
                        }
                        else {
                            if (i == (loopLength - 1)) {
                                sql.commit((err) => {
                                    if (err) {
                                        sql.rollback(function () {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err.message + "~From commit"
                                            });
                                        });
                                    }
                                    else {
                                        if (resContent) {
                                            res.send({
                                                status: 200,
                                                content: [],
                                                message: resContent
                                            });
                                        }
                                        else {
                                            res.send({
                                                status: 200,
                                                content: [],
                                                message: "New Schedules were added:)"
                                            });
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            }
        }
    })
})


router.get("/datesinschedules", (req, res) => {
    sql.query(`SELECT DISTINCT date FROM t_daily_schedules`, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/gettypeofexams", (req, res) => {
    sql.query('SELECT * FROM t_exams', (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {

            res.send({
                status: 200,
                content: result,
                message: "Successfully fetched data"
            })
        }
    })
})

router.post("/listofsections", (req, res) => {
    let id = req.body.id;
    let sqlQuery = 'SELECT * FROM t_class c WHERE c.std_fk = ?';
    sql.query(sqlQuery, id, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/listofsubjects", (req, res) => {
    let sections = req.body.sections;
    let sectionParams = null;
    if (sections.length > 0) {
        sectionParams = sections.join(",");
    }

    let sqlQuery = 'SELECT DISTINCT cs.group_id,cs.subject_fk,s.subject_name\
                        FROM t_class_subjects cs\
                        INNER JOIN t_subject s ON s.subject_id = cs.subject_fk\
                        WHERE cs.class_fk IN (?) ORDER BY cs.group_id';
    sql.query(sqlQuery, sectionParams, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/insertexamschedules", (req, res) => {
    let request = req.body;
    let schedulequery = [];
    let examquery = [];
    let allDates = [];
    let allSections = request.selectedSections;

    request.selectedSections.forEach(section => {
        request.scheduleInfo.forEach(subject => {

            let query1 = "INSERT INTO t_daily_schedules (session_id, date, class_fk, schedule_fk, period_fk, subject_fk, status_flag) \
                          VALUES (NULL,'"+ dateFormat(subject.date, "isoDate") + "'," + section + ",6," + subject.session + "," + subject.subject + ",NULL)";
            
            let query2 = "INSERT INTO t_exam_schedules(exam_schedule_id, session_fk, exam_fk, class_fk, group_fk,report_group, subject_fk, date, period_fk, max_marks,pass_marks,syllabus, status_flag)\
                        SELECT NULL, ? , "+ request.examName + ",cs.class_fk,cs.group_id,NULL,cs.subject_fk,'" + dateFormat(subject.date, "isoDate") + "'," + subject.session + "," + subject.maxMarks + ",'0'," + subject.syllabus + ",NULL FROM t_class_subjects cs WHERE cs.subject_fk = " + subject.subject + " AND cs.class_fk = " + section + ";"
            schedulequery.push(query1);
            examquery.push(query2);
            if(!allDates.includes(dateFormat(subject.date, "isoDate"))){
                allDates.push("'" + dateFormat(subject.date, "isoDate").toString()+ "'");
            }
        })
    });

    // console.log(schedulequery);
    // console.log(examquery);
    let loopLength = schedulequery.length;
    let checker = "SELECT COUNT(date) as counter FROM `t_daily_schedules` WHERE class_fk IN (" + allSections.join(",") + ") AND date IN (" + allDates.join(",") + ");";
    var resFlag = true;
    let resContent;

    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message + "From Begin Transaction"
            });
        }
        else {
            sql.query(checker, (err, result) => {
                if (err) {
                    res.send({
                        status: 500,
                        content: [],
                        message: err.message + "From Checker Transaction"
                    });
                }
                else {
                    console.log(result[0].counter);
                    if (result[0].counter) {
                        res.send({
                            status: 500,
                            content: [],
                            message: "Already schedules exists for this time frames make sure no schedules available"
                        });
                    }
                    else {
                        for (let i = 0; i < loopLength; i++) {
                            if (resFlag) {
                                sql.query(schedulequery[i], (err, result1) => {
                                    if (err) {
                                        sql.rollback(() => {
                                            if (!resContent) {
                                                resContent = err.message + "~From insert in daily schedule";
                                            }
                                        })
                                    }
                                    else {
                                        sql.query(examquery[i], result1.insertId, (err, result2) => {
                                            if (err) {
                                                sql.rollback(() => {
                                                    if (!resContent) {
                                                        resContent = err.message + "~From insert in exam schedule";
                                                    }
                                                })
                                            }
                                            else {
                                                if (i == (loopLength - 1)) {
                                                    sql.commit((err) => {
                                                        if (err) {
                                                            sql.rollback(function () {
                                                                res.send({
                                                                    status: 500,
                                                                    content: [],
                                                                    message: err.message + "~From commit"
                                                                });
                                                            });
                                                        }
                                                        else {
                                                            if (resContent) {
                                                                res.send({
                                                                    status: 200,
                                                                    content: [],
                                                                    message: resContent
                                                                });
                                                            }
                                                            else {
                                                                res.send({
                                                                    status: 200,
                                                                    content: [],
                                                                    message: "New Schedules were added:)"
                                                                });
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        })
                                    }
                                })
                            }
                        }
                    }
                }
            })
        }
    })
})


module.exports = router;