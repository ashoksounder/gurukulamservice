const express = require('express');
const router = express.Router();
const sql = require('../db.js');
const dateTime = require('date-time');
const getDateFromISO = function (today) {
    let year = today.getFullYear();
    let month = today.getMonth() + 1;
    let dt = today.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return year + '-' + month + '-' + dt;
}


router.get("/classlist", (req, res) => {
    let sqlQuery = 'SELECT * FROM t_class c INNER JOIN t_standard st WHERE c.std_fk = st.std_id AND c.class_teacher_fk = "?"';
    sql.query(sqlQuery, req.user.userId, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

//routes for standards form
router.get("/myclass/:id", (req, res) => {
    let id = req.params.id;
    console.log(id);
    let sqlQuery = 'SELECT cs.class_fk,cs.group_id,cs.group_name,cs.subject_fk,cs.subject_teacher_fk,s.subject_id,s.subject_name,s.activity_flag,ud.first_name,ud.last_name FROM t_class_subjects cs INNER JOIN  t_subject s ON cs.subject_fk = s.subject_id INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk AND cs.class_fk = ?';
    //SELECT * FROM t_class c INNER JOIN t_standard st WHERE c.std_fk = st.std_id AND c.class_teacher_fk = "?"
    //SELECT * FROM t_class_subjects cs INNER JOIN  t_subject s WHERE cs.subject_fk = s.subject_id AND cs.class_fk = "?"
    sql.query(sqlQuery, id, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/username", (req, res) => {
    let value = req.body.userName;
    console.log(value);
    let sqlQuery = 'SELECT user_id_fk,user_name FROM t_user d WHERE d.user_name = ?';
    sql.query(sqlQuery, value, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/addnewstudent", (req, res) => {
    let p = req.body;
    let sqlQuery1 = "INSERT INTO t_user_data(user_id, first_name, last_name, dob, gender, degree,joindate, contact1, contact2, email, address1, address2, state, po_code, city) VALUES (?)";
    let sqlQuery2 = "INSERT INTO t_user(user_name, password, role_fk, user_id_fk) VALUES (?)";
    let sqlQuery3 = "INSERT INTO t_student_subjects(student_fk, subject_fk) VALUES ?";
    let sqlQuery4 = "INSERT INTO t_student_class (student_fk, class_fk) VALUES (?)";
    let values1 = ['NULL', p.firstName, p.lastName, p.dob, p.gender,
        'NULL', p.joindate, p.primaryContact, p.secondaryContact,
        p.mail, p.address1, p.address2,
        p.state, p.postcode, p.city];

    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message + "From Transaction"
            })
        }
        sql.query(sqlQuery1, [values1], (err1, result1) => {
            if (err1) {
                sql.rollback(() => {
                    res.send({
                        status: 500,
                        content: [],
                        message: err1.message + "~From Insert in user data"
                    })
                })
            }
            else {
                let values2 = [p.userName, p.password, "4", result1.insertId];
                sql.query(sqlQuery2, [values2], (err2, result2) => {
                    if (err2) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err2.message + "~From Insert in users"
                            })
                        })
                    }
                    else {
                        let values3 = [];
                        let element;
                        for (let i = 0; i < p.subjects.length; i++) {
                            element = [];
                            element = [result1.insertId, p.subjects[i]];
                            values3.push(element);
                        }
                        console.log(values3);
                        sql.query(sqlQuery3, [values3], (err3, result3) => {
                            if (err3) {
                                sql.rollback(() => {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err3.message + "~From Insert in Subjects"
                                    })
                                })
                            }
                            else {
                                let values4 = [result1.insertId, p.class_id];
                                sql.query(sqlQuery4, [values4], (err4, result4) => {
                                    if (err4) {
                                        sql.rollback(() => {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err4.message + "~From Insert in class"
                                            })
                                        })
                                    }
                                    else {
                                        sql.commit((err5) => {
                                            if (err4) {
                                                sql.rollback(function () {
                                                    res.send({
                                                        status: 500,
                                                        content: [],
                                                        message: err5.message + "~From Insert in commit"
                                                    })
                                                });
                                            }
                                            res.send({
                                                status: 200,
                                                content: result1,
                                                message: "New Student added and subjects were mapped :)"
                                            })
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })
})

router.get("/listofdays", (req, res) => {
    let sqlQuery = 'SELECT * FROM t_days ORDER BY day_id ASC';
    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/listofperiods", (req, res) => {
    let sqlQuery = 'SELECT * FROM t_periods ORDER BY period_id ASC';
    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/classgroups/:id", (req, res) => {
    let id = req.params.id;
    console.log(id);
    let sqlQuery = 'SELECT cs.subject_fk,cs.class_fk,cs.group_id,cs.group_name,cs.subject_teacher_fk,s.subject_name,s.activity_flag,ud.first_name,ud.last_name FROM t_class_subjects cs INNER JOIN t_subject s ON cs.subject_fk = s.subject_id INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk WHERE cs.class_fk = ?';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/addschedule", (req, res) => {
    let s = req.body;
    let sqlQuery = "INSERT INTO t_class_periods(class_fk, day_fk, period_fk, group_fk)  VALUES (?) ON DUPLICATE KEY UPDATE group_fk = ?";
    let values = [s.class_id, s.day, s.period, s.group];
    let value2 = s.group;
    console.log(values);
    sql.query(sqlQuery, [values, value2], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/getschedule", (req, res) => {
    let value = req.body.id;
    console.log(value);
    let sqlQuery = 'SELECT DISTINCT cp.class_fk,cp.day_fk,cp.period_fk,p.period_name,cp.group_fk,cs.group_name FROM t_class_periods cp INNER JOIN t_class_subjects cs ON cp.group_fk = cs.group_id INNER JOIN t_periods p ON p.period_id = cp.period_fk WHERE cp.class_fk=? AND cs.class_fk=? ORDER BY cp.day_fk ASC, cp.period_fk ASC';
    //let sqlQuery = 'SELECT cp.class_fk,cp.day_fk,cp.period_fk,p.period_name,cp.group_fk,cs.group_name,cs.subject_fk,cs.subject_teacher_fk,ud.first_name, s.subject_name,s.activity_flag FROM t_class_periods cp INNER JOIN t_class_subjects cs ON cp.group_fk = cs.group_id INNER JOIN t_periods p ON p.period_id = cp.period_fk INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk INNER JOIN t_subject s ON s.subject_id = cs.subject_fk  WHERE cp.class_fk=? AND cs.class_fk=? ORDER BY cp.day_fk ASC, cp.period_fk ASC';
    sql.query(sqlQuery, [value, value], (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.get("/teachertodayschedule", (req, res) => {
    let prevDay = new Date(dateTime());
    prevDay.setDate(prevDay.getDate() - 1);
    let tommorow = new Date(dateTime());
    tommorow.setDate(tommorow.getDate() + 1);
    let todayFormat = getDateFromISO(new Date(dateTime()));
    let prevFormat = getDateFromISO(prevDay);
    let tomoFormat = getDateFromISO(tommorow);
    console.log(prevDay);

    let sqlQuery = "SELECT ds.session_id,DATE_FORMAT(ds.date, '%Y-%m-%d') as date,ds.schedule_fk,ds.status_flag,s.std_name,c.section_name,p.period_name,s.std_name,sb.subject_name,cs.subject_teacher_fk ,ud.first_name,ud.last_name,ud.dob\
    FROM t_daily_schedules ds\
    INNER JOIN t_class_subjects cs ON cs.class_fk = ds.class_fk AND cs.subject_fk = ds.subject_fk\
    INNER JOIN t_subject sb ON sb.subject_id = ds.subject_fk\
    INNER JOIN t_class c ON c.class_id = ds.class_fk\
    INNER JOIN t_standard s ON s.std_id = c.std_fk\
    INNER JOIN t_periods p ON p.period_id = ds.period_fk\
    INNER JOIN t_user_data ud ON ud.user_id=cs.subject_teacher_fk\
    WHERE cs.subject_teacher_fk = '"+ req.user.userId + "' AND ds.date between'" + prevFormat + "' AND '" + tomoFormat + "' ORDER BY ds.date ASC,p.period_id ASC;";

    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: {
                    schedules: result,
                    interval: [prevFormat, todayFormat, tomoFormat]
                },
                message: "Success"
            })
        }
    })
})

router.get("/teacherallschedules", (req, res) => {

    let date = getDateFromISO(new Date(dateTime()));
    let sqlQuery = "SELECT ds.session_id,sc.schedule_name,DATE_FORMAT(ds.date, '%Y-%m-%d') as date,ds.schedule_fk,ds.status_flag,s.std_name,c.section_name,p.period_name,s.std_name,sb.subject_name,cs.subject_teacher_fk ,ud.first_name,ud.last_name,ud.dob\
    FROM t_daily_schedules ds\
    INNER JOIN t_schedules sc ON sc.schedule_id = ds.schedule_fk\
    INNER JOIN t_class_subjects cs ON cs.class_fk = ds.class_fk AND cs.subject_fk = ds.subject_fk\
    INNER JOIN t_subject sb ON sb.subject_id = ds.subject_fk\
    INNER JOIN t_class c ON c.class_id = ds.class_fk\
    INNER JOIN t_standard s ON s.std_id = c.std_fk\
    INNER JOIN t_periods p ON p.period_id = ds.period_fk\
    INNER JOIN t_user_data ud ON ud.user_id=cs.subject_teacher_fk\
    WHERE cs.subject_teacher_fk = '"+ req.user.userId + "' AND ds.date <= '" + date + "' ORDER BY ds.date DESC,p.period_id ASC;";

    console.log(sqlQuery);

    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/sessioninfo/:id", (req, res) => {
    let id = req.params.id;
    console.log(id);
    let sqlQuery = "SELECT ds.session_id,DATE_FORMAT(ds.date, '%Y-%m-%d') as date,ds.schedule_fk,p.period_id,ds.status_flag,sd.session_info,sd.session_homework,sd.session_abs_count,c.class_id,s.std_name,sa.abort_reason,sa.acceptance_flag,c.section_name,p.period_name,s.std_name,cs.group_id,sb.subject_name,cs.subject_teacher_fk ,ud.first_name,ud.last_name,ud.dob\
    FROM t_daily_schedules ds\
    INNER JOIN t_class_subjects cs ON cs.class_fk = ds.class_fk AND cs.subject_fk = ds.subject_fk\
    INNER JOIN t_subject sb ON sb.subject_id = ds.subject_fk\
    INNER JOIN t_class c ON c.class_id = ds.class_fk\
    INNER JOIN t_standard s ON s.std_id = c.std_fk\
    INNER JOIN t_periods p ON p.period_id = ds.period_fk\
    INNER JOIN t_user_data ud ON ud.user_id=cs.subject_teacher_fk\
    LEFT JOIN t_session_details sd ON sd.session_fk = ds.session_id\
    LEFT JOIN t_session_abort_info sa ON sa.session_fk = ds.session_id\
    WHERE ds.session_id = "+ id + ";";
    console.log(sqlQuery);
    sql.query(sqlQuery, id, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.get("/sessionactionlist", (req, res) => {
    let sqlQuery = 'SELECT * FROM t_session_descriptions';
    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.get("/sessionactionlist", (req, res) => {
    let sqlQuery = 'SELECT * FROM t_session_descriptions';
    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.post("/sessionstudents", (req, res) => {
    let value = req.body.id;
    console.log(value);
    let sqlQuery = "SELECT ss.student_fk,CONCAT(ud.first_name,' ',ud.last_name) as fullname FROM t_daily_schedules ds \
    INNER JOIN t_student_class sc ON sc.class_fk = ds.class_fk \
    INNER JOIN t_student_subjects ss ON ss.subject_fk = ds.subject_fk AND ss.student_fk = sc.student_fk \
    INNER JOIN t_user_data ud ON ud.user_id = sc.student_fk \
    WHERE ds.session_id = ? ORDER BY ud.first_name ASC";
    //let sqlQuery = 'SELECT cp.class_fk,cp.day_fk,cp.period_fk,p.period_name,cp.group_fk,cs.group_name,cs.subject_fk,cs.subject_teacher_fk,ud.first_name, s.subject_name,s.activity_flag FROM t_class_periods cp INNER JOIN t_class_subjects cs ON cp.group_fk = cs.group_id INNER JOIN t_periods p ON p.period_id = cp.period_fk INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk INNER JOIN t_subject s ON s.subject_id = cs.subject_fk  WHERE cp.class_fk=? AND cs.class_fk=? ORDER BY cp.day_fk ASC, cp.period_fk ASC';
    sql.query(sqlQuery, value, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/classgroups", (req, res) => {
    let id = req.body.id;
    console.log(id);
    let sqlQuery = 'SELECT cs.subject_fk,cs.class_fk,cs.group_id,cs.group_name,cs.subject_teacher_fk,s.subject_name,s.activity_flag,ud.first_name,ud.last_name FROM t_class_subjects cs INNER JOIN t_subject s ON cs.subject_fk = s.subject_id INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk WHERE cs.class_fk = ?';
    sql.query(sqlQuery, id, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/sessiondatafeed", (req, res) => {
    let data = req.body;
    console.log(data);
    switch (data.actionType) {
        case 1:
            console.log("triggered");
            // res.send({});
            let sqlQuery1 = "INSERT INTO t_session_details(session_fk, session_info, session_homework, session_abs_count) VALUES (?) \
                         ON DUPLICATE KEY UPDATE session_info=VALUES(session_info),session_homework=VALUES(session_homework),session_abs_count=VALUES(session_abs_count)";
            let sqlDeleteQuery = "DELETE FROM t_session_absentees WHERE session_fk = ?";
            let sqlQuery3 = "INSERT INTO t_session_absentees(session_fk, abs_fk, abs_flag) VALUES ?";
            let sqlQuery2 = "UPDATE t_daily_schedules SET status_flag=? WHERE session_id=?";
            let values1 = [data.session_id, data.sessionDescription, data.homeWork, data.abs_count];
            let values3 = [];
            let element;
            for (let i = 0; i < data.abs_count; i++) {
                element = [];
                element = [data.session_id, data.absentees[i], 1];
                values3.push(element);
            }
            sql.beginTransaction(function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        content: [],
                        message: err.message
                    })
                }
                sql.query(sqlQuery1, [values1], (err1, result1) => {
                    if (err1) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err1.message
                            })
                        })
                    }
                    else {
                        sql.query(sqlDeleteQuery, data.session_id, (errD, result2) => {
                            if (errD) {
                                sql.rollback(() => {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: errD.message
                                    })
                                })
                            }
                            else {
                                console.log("step2");
                                sql.query(sqlQuery2, [data.actionType, data.session_id], (err2, result2) => {
                                    if (err2) {
                                        sql.rollback(() => {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err2.message
                                            })
                                        })
                                    }
                                    else {
                                        //if will be executed when absentees are there :(
                                        if (data.abs_count) {
                                            sql.query(sqlQuery3, [values3], (err3, result3) => {
                                                if (err3) {
                                                    sql.rollback(() => {
                                                        res.send({
                                                            status: 500,
                                                            content: [],
                                                            message: err3.message
                                                        })
                                                    })
                                                }
                                                else {
                                                    sql.commit((err4) => {
                                                        if (err4) {
                                                            sql.rollback(function () {
                                                                res.send({
                                                                    status: 500,
                                                                    content: [],
                                                                    message: err4.message
                                                                })
                                                            });
                                                        }
                                                        res.send({
                                                            status: 200,
                                                            content: result1,
                                                            message: "Successfully posted the data"
                                                        })
                                                    })
                                                }
                                            })
                                        }
                                        else {
                                            sql.commit((err4) => {
                                                if (err4) {
                                                    sql.rollback(function () {
                                                        res.send({
                                                            status: 500,
                                                            content: [],
                                                            message: err4.message
                                                        })
                                                    });
                                                }
                                                res.send({
                                                    status: 200,
                                                    content: result1,
                                                    message: "Successfully posted the data"
                                                })
                                            })
                                        }
                                    }
                                })
                            }
                        })
                    }
                })
            })
            break;

        case 2:
            let sqlQuery4 = "INSERT INTO t_session_swap_manage(swap_id, session_fk, class_fk, swap_from, swap_to, initiator_fk, acceptor_fk, swap_status_fk) VALUES (?)";
            // let sqlQuery5 = "UPDATE t_daily_schedules SET status_flag=? WHERE session_id=?";
            let sqlQuery5 = "UPDATE t_daily_schedules SET status_flag=? WHERE date=? AND class_fk=? AND period_fk=? ";
            let values4 = ['NULL', data.session_id, data.class_id, data.group_id, data.swapTo, data.subject_teacher_fk, null, 1];
            let values5 = [2, data.date, data.class_id, data.period_id];
            sql.beginTransaction(function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        content: [],
                        message: err.message
                    })
                }
                sql.query(sqlQuery4, [values4], (err1, result1) => {
                    if (err1) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err1.message
                            })
                        })
                    }
                    else {
                        sql.query(sqlQuery5, values5, (err2, result2) => {
                            if (err2) {
                                sql.rollback(() => {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err2.message
                                    })
                                })
                            }
                            else {
                                sql.commit((err3) => {
                                    if (err3) {
                                        sql.rollback(function () {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err3.message
                                            })
                                        });
                                    }
                                    res.send({
                                        status: 200,
                                        content: result1,
                                        message: "Successfully posted the data"
                                    })
                                })
                            }
                        })
                    }
                })
            })
            break;

        case 3:
            let sqlQuery6 = "INSERT INTO t_session_abort_info(session_fk, abort_reason, acceptance_flag) VALUES (?)";
            let sqlQuery7 = "UPDATE t_daily_schedules SET status_flag=? WHERE session_id=?";
            let values6 = [data.session_id, data.reasonToAbort, 0];
            sql.beginTransaction(function (err) {
                if (err) {
                    res.send({
                        status: 500,
                        content: [],
                        message: err.message
                    })
                }
                sql.query(sqlQuery6, [values6], (err1, result1) => {
                    if (err1) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err1.message
                            })
                        })
                    }
                    else {
                        sql.query(sqlQuery7, [data.actionType, data.session_id], (err2, result2) => {
                            if (err2) {
                                sql.rollback(() => {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err2.message
                                    })
                                })
                            }
                            else {
                                sql.commit((err3) => {
                                    if (err3) {
                                        sql.rollback(function () {
                                            res.send({
                                                status: 500,
                                                content: [],
                                                message: err3.message
                                            })
                                        });
                                    }
                                    res.send({
                                        status: 200,
                                        content: result1,
                                        message: "Successfully posted the data"
                                    })
                                })
                            }
                        })
                    }
                })
            })
            break;

        default:
            res.send({
                status: 500,
                content: [],
                message: "No action happened"
            })
    }
})

router.post("/getsessionabsentees", (req, res) => {
    let id = req.body.id;
    let sqlQuery = 'SELECT * FROM t_session_absentees WHERE session_fk = ?';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/getswapinfo", (req, res) => {
    let id = req.body.id;
    let sqlQuery = 'SELECT * FROM t_session_swap_manage WHERE session_fk = ?';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/updateswap", (req, res) => {
    data = req.body;
    let acceptorId = null;
    if (data.swap_status == 2 || data.swap_status == 3) {
        acceptorId = req.user.userId;
    }
    if (data.swap_status == 2) {
        console.log(data);
        let sqlQuery1 = "UPDATE t_session_swap_manage SET acceptor_fk=?,swap_status_fk = ? WHERE swap_id = ?";
        let values1 = [acceptorId, data.swap_status, data.swap_id];
        let sqlQuery2 = "UPDATE t_daily_schedules SET status_flag=? WHERE date=? AND class_fk=? AND period_fk=?";
        let values2 = [9, data.date, data.class_id, data.period_id];
        let sqlQuery3 = "INSERT INTO t_daily_schedules (session_id,date,class_fk, schedule_fk, period_fk, subject_fk, status_flag) \
                         SELECT NULL,?,cs.class_fk,1,?,cs.subject_fk,NULL FROM t_class_subjects cs WHERE cs.group_id= ? AND cs.class_fk=?";
        let values3 = [data.date, data.period_id, data.group_id, data.class_id];
        sql.beginTransaction(function (err) {
            if (err) {
                res.send({
                    status: 500,
                    content: [],
                    message: err.message
                })
            }
            sql.query(sqlQuery1, values1, (err1, result1) => {
                if (err1) {
                    sql.rollback(() => {
                        res.send({
                            status: 500,
                            content: [],
                            message: err1.message
                        })
                    })
                }
                else {
                    sql.query(sqlQuery2, values2, (err2, result2) => {
                        if (err2) {
                            sql.rollback(() => {
                                res.send({
                                    status: 500,
                                    content: [],
                                    message: err2.message
                                })
                            })
                        }
                        else {
                            sql.query(sqlQuery3, values3, (err3, result3) => {
                                if (err2) {
                                    sql.rollback(() => {
                                        res.send({
                                            status: 500,
                                            content: [],
                                            message: err2.message
                                        })
                                    })
                                }
                                else {
                                    sql.commit((err4) => {
                                        if (err4) {
                                            sql.rollback(function () {
                                                res.send({
                                                    status: 500,
                                                    content: [],
                                                    message: err4.message
                                                })
                                            });
                                        }
                                        res.send({
                                            status: 200,
                                            content: result3,
                                            message: "Your swap update is successfull !!!"
                                        })
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })

    }
    else {
        let sqlQuery1 = "UPDATE t_session_swap_manage SET acceptor_fk=?,swap_status_fk = ? WHERE swap_id = ?";
        let sqlQuery2 = "UPDATE t_daily_schedules SET status_flag=? WHERE date=? AND class_fk=? AND period_fk=?";
        let values1 = [acceptorId, data.swap_status, data.swap_id];
        let values2 = [data.status_flag, data.date, data.class_id, data.period_id];
        sql.beginTransaction(function (err) {
            if (err) {
                res.send({
                    status: 500,
                    content: [],
                    message: err.message
                })
            }
            sql.query(sqlQuery1, values1, (err1, result1) => {
                if (err1) {
                    sql.rollback(() => {
                        res.send({
                            status: 500,
                            content: [],
                            message: err1.message
                        })
                    })
                }
                else {
                    sql.query(sqlQuery2, values2, (err2, result2) => {
                        if (err2) {
                            sql.rollback(() => {
                                res.send({
                                    status: 500,
                                    content: [],
                                    message: err2.message
                                })
                            })
                        }
                        else {
                            sql.commit((err3) => {
                                if (err3) {
                                    sql.rollback(function () {
                                        res.send({
                                            status: 500,
                                            content: [],
                                            message: err3.message
                                        })
                                    });
                                }
                                res.send({
                                    status: 200,
                                    content: result1,
                                    message: "Your swap update is successfull !!!"
                                })
                            })
                        }
                    })
                }
            })
        })
    }
})

router.post("/cancelabort", (req, res) => {
    id = req.body.id;
    let sqlQuery1 = "DELETE FROM t_session_abort_info WHERE session_fk = ?";
    let sqlQuery2 = "UPDATE t_daily_schedules SET status_flag=null WHERE session_id=?";
    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        sql.query(sqlQuery1, id, (err1, result1) => {
            if (err1) {
                sql.rollback(() => {
                    res.send({
                        status: 500,
                        content: [],
                        message: err1.message
                    })
                })
            }
            else {
                sql.query(sqlQuery2, id, (err2, result2) => {
                    if (err2) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err2.message
                            })
                        })
                    }
                    else {
                        sql.commit((err3) => {
                            if (err3) {
                                sql.rollback(function () {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err3.message
                                    })
                                });
                            }
                            res.send({
                                status: 200,
                                content: result1,
                                message: "Your abort request has been cancelled !!!"
                            })
                        })
                    }
                })
            }
        })
    })
})

router.post("/listofstudents", (req, res) => {
    let id = req.body.id;
    let sqlQuery = 'SELECT cs.class_fk,ud.user_id,ud.first_name,ud.last_name,ud.joindate,ud.dob,ud.gender  FROM t_student_class cs INNER JOIN t_user_data ud ON ud.user_id = cs.student_fk WHERE cs.class_fk = ?';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/swapapprovalsopen", (req, res) => {
    let id = req.user.userId;
    let sqlQuery = 'SELECT ss.swap_id,ds.date,ss.session_fk,s.subject_name,ss.swap_status_fk,st.std_name,c.section_name, p.period_name FROM t_session_swap_manage ss INNER JOIN t_class_subjects cs ON cs.class_fk = ss.class_fk AND cs.group_id = ss.swap_to INNER JOIN t_class c ON c.class_id = ss.class_fk INNER JOIN t_subject s ON s.subject_id = cs.subject_fk INNER JOIN t_standard st ON st.std_id = c.std_fk INNER JOIN t_daily_schedules ds ON ds.session_id = ss.session_fk INNER JOIN t_periods p ON p.period_id=ds.period_fk WHERE cs.subject_teacher_fk  = ? AND ss.swap_status_fk = 1';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.get("/swapdetailed", (req, res) => {
    let id = req.user.userId;
    let sqlQuery = "SELECT ss.swap_id,ss.initiator_fk,ss.swap_to,c.class_id,p.period_id,CONCAT(ud.first_name,' ',ud.last_name) as requestor,DATE_FORMAT(ds.date, '%Y-%m-%d') as date,ss.session_fk,s.subject_name,ss.swap_status_fk,st.std_name,c.section_name, p.period_name FROM t_session_swap_manage ss INNER JOIN t_class_subjects cs ON cs.class_fk = ss.class_fk AND cs.group_id = ss.swap_to INNER JOIN t_class c ON c.class_id = ss.class_fk INNER JOIN t_subject s ON s.subject_id = cs.subject_fk INNER JOIN t_standard st ON st.std_id = c.std_fk INNER JOIN t_daily_schedules ds ON ds.session_id = ss.session_fk INNER JOIN t_periods p ON p.period_id=ds.period_fk INNER JOIN t_user_data ud ON ud.user_id=ss.initiator_fk WHERE cs.subject_teacher_fk  = ? ORDER BY ds.date";
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.post("/countofclassperiods", (req, res) => {
    let id = req.body.id;
    let sqlQuery = 'Select class_fk, day_fk,count(period_fk) as session_count from t_class_periods WHERE class_fk = ? group by class_fk,day_fk ORDER BY day_fk ASC';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.get("/teacherexamreports", (req, res) => {

    let date = getDateFromISO(new Date(dateTime()));
    let sqlQuery = "SELECT es.exam_schedule_id,es.session_fk,DATE_FORMAT(es.date, '%Y-%m-%d') as date,e.exam_name,es.status_flag,s.std_name,c.section_name,p.period_name,s.std_name,sb.subject_name,cs.subject_teacher_fk ,ud.first_name,ud.last_name,ud.dob\
    FROM t_exam_schedules es\
    INNER JOIN t_exams e ON e.exam_id = es.exam_fk\
    INNER JOIN t_class_subjects cs ON cs.class_fk = es.class_fk AND cs.subject_fk = es.subject_fk\
    INNER JOIN t_subject sb ON sb.subject_id = es.subject_fk\
    INNER JOIN t_class c ON c.class_id = es.class_fk\
    INNER JOIN t_standard s ON s.std_id = c.std_fk\
    INNER JOIN t_periods p ON p.period_id = es.period_fk\
    INNER JOIN t_user_data ud ON ud.user_id=cs.subject_teacher_fk\
    WHERE cs.subject_teacher_fk = '"+ req.user.userId + "' AND es.date <= '" + date + "' ORDER BY es.date DESC,p.period_id ASC";


    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/getdetailedexamreport", (req, res) => {
    let id = req.body.id;
    let sqlQuery = 'SELECT st.std_name,ud.user_id,c.section_name,es.class_fk,e.exam_name,es.status_flag,CONCAT(ud.first_name," ",ud.last_name) as fullName, es.max_marks,s.subject_name,er.mark_scored,er.comments,sa.abs_flag\
    FROM t_exam_schedules es \
    INNER JOIN t_class c ON c.class_id = es.class_fk \
    INNER JOIN t_exams e ON e.exam_id = es.exam_fk \
    INNER JOIN t_standard st ON st.std_id = c.std_fk \
    INNER JOIN t_student_class sc ON sc.class_fk = es.class_fk \
    INNER JOIN t_user_data ud ON ud.user_id = sc.student_fk \
    LEFT JOIN t_session_absentees sa ON sa.session_fk = es.session_fk AND sa.abs_fk = ud.user_id \
    INNER JOIN t_student_subjects ss ON ss.student_fk = sc.student_fk AND ss.subject_fk = es.subject_fk \
    INNER JOIN t_subject s ON s.subject_id = es.subject_fk \
    LEFT JOIN t_exam_reports er ON er.student_fk = ud.user_id AND er.exam_schedule_fk = es.exam_schedule_id \
    WHERE es.exam_schedule_id = ? ORDER BY ud.first_name ASC';
    sql.query(sqlQuery, id, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/postexammarks", (req, res) => {
    id = req.body.id;
    let values1 = req.body.studentInfo;
    let sqlQuery1 = "INSERT INTO t_exam_reports (student_fk, exam_schedule_fk, mark_scored, comments, status_flag) VALUES ?\
                     ON DUPLICATE KEY UPDATE\
                     mark_scored = VALUES(mark_scored),\
                     comments = VALUES(comments),\
                     status_flag = VALUES(status_flag)";
    let sqlQuery2 = "UPDATE t_exam_schedules SET status_flag =1 WHERE exam_schedule_id = ?";
    console.log(sqlQuery1);
    console.log(values1);
    sql.beginTransaction(function (err) {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        sql.query(sqlQuery1, [values1], (err1, result1) => {
            if (err1) {
                sql.rollback(() => {
                    res.send({
                        status: 500,
                        content: [],
                        message: err1.message
                    })
                })
            }
            else {
                sql.query(sqlQuery2, id, (err2, result2) => {
                    if (err2) {
                        sql.rollback(() => {
                            res.send({
                                status: 500,
                                content: [],
                                message: err2.message
                            })
                        })
                    }
                    else {
                        sql.commit((err3) => {
                            if (err3) {
                                sql.rollback(function () {
                                    res.send({
                                        status: 500,
                                        content: [],
                                        message: err3.message
                                    })
                                });
                            }
                            res.send({
                                status: 200,
                                content: result1,
                                message: "Your exam reports were submitted !!!"
                            })
                        })
                    }
                })
            }
        })
    })
})

router.get("/getexamconfig", (req, res) => {

    let sqlQuery = 'Select t1.exam_fk,t1.exam_name,t1.std_name,t1.section_name,t1.class_fk from (Select e.exam_name,s.std_name,c.section_name,c.academic_fk,c.class_teacher_fk,es.exam_schedule_id,es.exam_fk,es.class_fk,Sum(status_flag) as TL1 from t_exam_schedules es \
    INNER JOIN t_exams e on es.exam_fk = e.exam_id \
    INNER JOIN t_class c ON c.class_id = es.class_fk \
    INNER JOIN t_standard s ON s.std_id = c.std_fk \
    INNER JOIN t_user_data u ON u.user_id = c.class_teacher_fk \
    WHERE u.user_id=? AND es.status_flag IS NOT NULL GROUP BY es.exam_fk,es.class_fk)  t1 \
    INNER JOIN \
    (Select e.exam_name,s.std_name,c.section_name,c.academic_fk,c.class_teacher_fk,es.exam_schedule_id,es.exam_fk,Sum(status_flag = 1) as TL2 from t_exam_schedules es \
    INNER JOIN t_exams e on es.exam_fk = e.exam_id \
    INNER JOIN t_class c ON c.class_id = es.class_fk \
    INNER JOIN t_standard s ON s.std_id = c.std_fk \
    INNER JOIN t_user_data u ON u.user_id = c.class_teacher_fk \
    WHERE u.user_id=? AND es.status_flag IS NOT NULL GROUP BY es.exam_fk,es.class_fk) t2 \
    on t1.exam_schedule_id = t2.exam_schedule_id AND t1.tl1=t2.tl2';

    sql.query(sqlQuery, [req.user.userId, req.user.userId], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})

router.post("/examconfdetail", (req, res) => {
    let class_id = req.body.class_id;
    let exam_id = req.body.exam_id;
    let sqlQuery = 'SELECT es.exam_schedule_id,es.max_marks,es.pass_marks,es.report_group,s.subject_id,s.subject_name,cs.group_name,cs.group_id,e.exam_name,CONCAT(st.std_name , "-", c.section_name) as classname FROM t_exam_schedules es \
                    INNER JOIN t_class c ON c.class_id = es.class_fk\
                    INNER JOIN t_standard st ON st.std_id = c.std_fk\
                    INNER JOIN t_exams e ON e.exam_id = es.exam_fk\
                    INNER JOIN t_subject s ON s.subject_id = es.subject_fk \
                    INNER JOIN t_class_subjects cs ON cs.subject_fk = s.subject_id AND cs.class_fk = es.class_fk \
                    WHERE es.exam_fk = ? AND es.class_fk = ?';
    sql.query(sqlQuery, [exam_id, class_id], (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.get("/getexamschedule", (req, res) => {
    let id = req.user.userId;
    let sqlQuery = 'SELECT es.exam_schedule_id,es.class_fk,e.exam_id,s.subject_name,e.exam_name,es.subject_fk,es.date,p.period_name,es.max_marks,es.syllabus,es.status_flag,CONCAT(st.std_name, "-" , c.section_name) as class_name,ed.exam_status_description  FROM t_exam_schedules es \
                    INNER JOIN t_exams e on e.exam_id = es.exam_fk \
                    INNER JOIN t_subject s on s.subject_id = es.subject_fk\
                    INNER JOIN t_daily_schedules ds on ds.session_id = es.session_fk \
                    INNER JOIN t_periods p on p.period_id = ds.period_fk \
                    INNER JOIN t_class c on c.class_id = es.class_fk\
                    INNER JOIN t_standard st on st.std_id = c.std_fk\
                    LEFT JOIN t_exam_descriptions ed on es.status_flag = ed.exam_status_id\
                    WHERE c.class_teacher_fk = ? \
                    ORDER BY es.date ASC,es.period_fk ASC';
    //let sqlQuery = 'SELECT cp.class_fk,cp.day_fk,cp.period_fk,p.period_name,cp.group_fk,cs.group_name,cs.subject_fk,cs.subject_teacher_fk,ud.first_name, s.subject_name,s.activity_flag FROM t_class_periods cp INNER JOIN t_class_subjects cs ON cp.group_fk = cs.group_id INNER JOIN t_periods p ON p.period_id = cp.period_fk INNER JOIN t_user_data ud ON ud.user_id = cs.subject_teacher_fk INNER JOIN t_subject s ON s.subject_id = cs.subject_fk  WHERE cp.class_fk=? AND cs.class_fk=? ORDER BY cp.day_fk ASC, cp.period_fk ASC';
    sql.query(sqlQuery, id, (err, result) => {
        console.log(result);
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});


router.post("/postexamconf", (req, res) => {
    let data = req.body;
    let sqlQuery = '';
    data.forEach(subject => {
        let tempQuery = '';
        tempQuery = 'UPDATE t_exam_schedules SET report_group=' + subject.reportGroup + ',pass_marks=' + subject.passMarks + ',status_flag=2 WHERE exam_schedule_id =' + subject.id + "; "
        sqlQuery = sqlQuery + tempQuery;
    });
    console.log(sqlQuery);
    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
})


router.post("/classexamreport", (req, res) => {
    let class_id = parseInt(req.body.class_id);
    let exam_id = parseInt(req.body.exam_id);

    let sqlQuery = 'SELECT  q1.student_fk,q1.fullname,er.exam_schedule_fk,q1.subject_fk,q1.subject_name,q1.report_group,q1.group_name,q1.pass_marks,q1.mark_scored,q1.group_total,q1.group_max,q1.pass_flag,q3.total,q3.failcounter,q2.rank FROM t_exam_reports er\
    LEFT JOIN\
    (SELECT er.student_fk, sq.fullname,es.report_group,sq.group_name,es.subject_fk,s.subject_name,es.exam_schedule_id,er.mark_scored,es.pass_marks,sq.group_total,sq.group_max,sq.pass_flag FROM t_exam_reports er INNER JOIN t_exam_schedules es ON es.exam_schedule_id = er.exam_schedule_fk \
    INNER JOIN t_subject s ON s.subject_id = es.subject_fk LEFT JOIN (SELECT sqq.student_fk, sqq.fullname,sqq.report_group,sqq.group_name,sqq.exam_schedule_id,sqq.class_fk,sqq.exam_fk,sqq.mark_scored,sqq.pass_marks,SUM(sqq.mark_scored) as group_total,SUM(sqq.max_marks) as group_max, SUM(sqq.mark_scored) >= sqq.pass_marks AS pass_flag \
    FROM(SELECT DISTINCT er.student_fk,CONCAT(ud.first_name," ",ud.last_name) as fullname,es.report_group,cs.group_name,es.exam_schedule_id,es.class_fk,es.exam_fk,er.mark_scored,es.pass_marks,ss.subject_fk,es.max_marks FROM t_exam_schedules es \
    INNER JOIN t_exam_reports er ON er.exam_schedule_fk = es.exam_schedule_id \
    INNER JOIN t_user_data ud ON ud.user_id = er.student_fk \
    INNER JOIN t_class_subjects cs ON cs.group_id = es.report_group AND cs.class_fk = es.class_fk \
    INNER JOIN t_student_subjects ss ON ss.student_fk = ud.user_id AND es.subject_fk = ss.subject_fk WHERE es.exam_fk = "'+ exam_id + '" AND es.class_fk = "' + class_id + '") sqq \
    GROUP BY sqq.student_fk,sqq.report_group) sq ON sq.student_fk = er.student_fk AND sq.report_group = es.report_group WHERE es.exam_fk = "'+ exam_id + '" AND es.class_fk = "' + class_id + '") Q1 ON er.student_fk = q1.student_fk AND q1.exam_schedule_id = er.exam_schedule_fk \
    INNER JOIN \
    (SELECT sb1.student_fk,SUM(sb1.pass_flag = 0) as failcounter,SUM(sb1.group_total) as total\
    FROM (SELECT er.student_fk,SUM(er.mark_scored) as group_total,es.pass_marks,SUM(er.mark_scored)>=es.pass_marks as pass_flag  FROM t_exam_reports er\
    INNER JOIN t_exam_schedules es ON es.exam_schedule_id = er.exam_schedule_fk WHERE es.exam_fk = "'+ exam_id + '" AND es.class_fk = "' + class_id + '" \
    GROUP BY er.student_fk,es.report_group) sb1 GROUP BY sb1.student_fk)Q3 ON q3.student_fk = q1.student_fk\
    LEFT JOIN\
    (SELECT student_fk,failcounter,total,\
    @curRank := IF(@prevVal=total, @curRank, @studentNumber) AS rank,\
    @studentNumber := @studentNumber + 1 as studentNumber,\
    @prevVal:=total as prev\
    FROM\
    (SELECT sb1.student_fk,SUM(sb1.pass_flag = 0) as failcounter,SUM(sb1.group_total) as total\
    FROM (SELECT er.student_fk,SUM(er.mark_scored) as group_total,es.pass_marks,SUM(er.mark_scored)>=es.pass_marks as pass_flag  FROM t_exam_reports er\
    INNER JOIN t_exam_schedules es ON es.exam_schedule_id = er.exam_schedule_fk WHERE es.exam_fk = "'+ exam_id + '" AND es.class_fk = "' + class_id + '"\
    GROUP BY er.student_fk,es.report_group) sb1 GROUP BY sb1.student_fk) q,(SELECT @curRank :=0, @prevVal:=null, @studentNumber:=1) r WHERE q.failcounter = 0 ORDER BY total DESC) Q2 ON q2.student_fk = q1.student_fk\
    ORDER BY q1.fullname ASC';

    console.log(sqlQuery);

    sql.query(sqlQuery, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});


router.get("/departmentwiseteachers", (req, res) => {

    let sqlQuery = 'SELECT ud.user_id,CONCAT(ud.first_name," ",ud.last_name) as fullname,r.role_name,d.dept_name,ud.dob,ud.degree FROM t_user_data ud \
    INNER JOIN t_user u ON u.user_id_fk=ud.user_id \
    INNER JOIN t_roles r ON r.role_id = u.role_fk \
    INNER JOIN t_teacher_departments td ON td.user_fk = ud.user_id \
    INNER JOIN t_department d ON d.dept_id = td.dept_fk \
    INNER JOIN (SELECT td.dept_fk FROM t_user u INNER JOIN t_teacher_departments td ON td.user_fk = u.user_id_fk WHERE u.user_id_fk =?) t2 \
    WHERE u.role_fk  IN (1,3) AND d.dept_id = t2.dept_fk ORDER BY fullname';

    console.log(sqlQuery);

    sql.query(sqlQuery, req.user.userId, (err, result) => {
        if (err) {
            res.send({
                status: 500,
                content: [],
                message: err.message
            })
        }
        else {
            res.send({
                status: 200,
                content: result,
                message: "Success"
            })
        }
    })
});

router.post("/teacherprofileinfo", (req, res) => {
    let teacher_id = req.body.teacher_id;

    sql.query(`SELECT ud.user_id,CONCAT(ud.first_name," ",ud.last_name)as fullname,ud.dob,ud.gender,ud.joindate,ud.contact1,ud.contact2,ud.email,ud.address1,ud.address2,ud.state,ud.po_code,ud.city,u.user_name,u.password,r.role_name,d.dept_name FROM t_user_data ud 
                INNER JOIN t_user u ON u.user_id_fk = ud.user_id 
                INNER JOIN t_roles r ON r.role_id = u.role_fk
                INNER JOIN t_teacher_departments td ON td.user_fk = ud.user_id
                INNER JOIN t_department d ON d.dept_id = td.dept_fk
                WHERE ud.user_id  = ?`, [teacher_id], (err, result) => {
            if (err) {
                res.send({
                    status: 500,
                    message: err.message,
                    content: []
                })
            }
            else {
                sql.query(`SELECT CONCAT(st.std_name,"-",c.section_name) as class_name,s.subject_name FROM t_class_subjects cs
                INNER JOIN t_subject s ON s.subject_id = cs.subject_fk
                INNER JOIN t_class c ON c.class_id = cs.class_fk
                INNER JOIN t_standard st ON st.std_id = c.std_fk
                WHERE cs.subject_teacher_fk = ?`, [teacher_id], (err1, result1) => {
                        if (err1) {
                            res.send({
                                status: 500,
                                message: err1.message,
                                content: []
                            })
                        }
                        else {
                            res.send({
                                status: 200,
                                message: "Success",
                                content: [result, result1]
                            })
                        }
                    });
            }
        });
});


module.exports = router;